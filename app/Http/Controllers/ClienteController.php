<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;

class ClienteController extends Controller
{
    //WR
    public function ListaCliente (Request $request)
    {
        {
            //dd('Hola mi nombre es Wilson Ramirez');
            $cliente = Cliente::all();
            return view('clientes.lista')->with('cliente', $cliente);
    
        }
    }

    public function CrearCliente (Request $request)
    {
        $cliente = Cliente::all();
        //dd('Hola mi nombre es Wilson Ramirez');
        return view('clientes.crearcliente')->with('cliente', $cliente);
    }


    public function GuardarClientes (Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required',
            'apellidos'=> 'required',
            'cedula'=> 'required',
            'direccion'=> 'required',
            'telefono'=> 'required',
            'fecha_nacimiento'=> 'required',
            'email'=> 'required'
        ]);

        $cliente = new Cliente; 
        $cliente->nombre    = $request->nombre;
        $cliente->apellidos    = $request->apellidos;
        $cliente->cedula    = $request->cedula;
        $cliente->direccion    = $request->direccion;
        $cliente->telefono    = $request->telefono;
        $cliente->fecha_nacimiento    = $request->fecha_nacimiento;
        $cliente->email    = $request->email;
        $cliente->save();
        
        return redirect()->route('lista.cliente');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
