<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function(){

    Route::post('/guardar/productos', ['as' => 'productos.guardar', 'uses' => 'ProductoController@GuardarProductos']);
    Route::get('/lista/productos', ['as' => 'list.productos', 'uses' => 'ProductoController@InicioProducto']);
    Route::get('/crear/productos', ['as' => 'crear.productos', 'uses' => 'ProductoController@CrearProducto']);
    
    Route::post('/guardar/clientes', ['as' => 'clientes.guardar', 'uses' => 'ClienteController@GuardarClientes']);
    Route::get('/lista/clientes', ['as' => 'lista.cliente', 'uses' => 'ClienteController@ListaCliente']);
    Route::get('/crear/clientes', ['as' => 'crear.clientes', 'uses' => 'ClienteController@CrearCliente']);
    
});