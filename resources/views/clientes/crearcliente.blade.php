@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('CREAR UN CLIENTE') }}</div>

            <div class="col text-right">
                <a href="{{route('lista.cliente')}}" class="btn btn-sm btn-success">Cancelar</a>
            </div>
                <div class="card-body">
                    
                    <form role="form" method="post" action="{{ route('clientes.guardar')}}">
                        {{ csrf_field() }}
                        {{ method_field ('post') }}

                        <div class="row">
                            <div class="col-lg-4">
                            <label class="from-control-label" for="nombre">Nombres</label>
                            <input type="text" class="from-control" name="nombre">
                            </div>

                            <div class="col-lg-4">
                            <label class="from-control-label" for="tipo">Apellidos</label>
                            <input type="text" class="from-control" name="apellidos">
                            </div>

                            <div class="col-lg-4">
                            <label class="from-control-label" for="estado">Cedula CC</label>
                            <input type="text" class="from-control" name="cedula">
                            </div>

                            <div class="col-lg-4">
                            <label class="from-control-label" for="tipo">Direccion Barrio</label>
                            <input type="text" class="from-control" name="direccion">
                            </div>

                            <div class="col-lg-4">
                            <label class="from-control-label" for="precio">N° Telefono</label>
                            <input type="text" class="from-control" name="telefono">
                            </div>

                            <div class="col-lg-4">
                            <label class="from-control-label" for="precio">Fecha de nacimiento</label>
                            <input type="date" class="from-control" name="fecha_nacimiento">
                            </div>

                            <div class="col-lg-4">
                            <label class="from-control-label" for="precio">E-mail</label>
                            <input type="text" class="from-control" name="email">
                            </div>
                            
                        </div>

                        <button type="submit" class="btn btn-succes pull-right"> Guardar </button>
                    </form>
                
                </div>
            </div>
        </div>
    </div>
</div>
@endsection